# Executable
In **/dist** folder

# Requirement
## Install NodeJS
NodeJS & NPM

## Install Modules
Install node modules
```
npm i
```

Start the application
```
npm start
```

Generate executable

```
npm build           # for all OS
npm build:platform  # for only current OS
```


# Git global setup

```
git config --global user.name "Thomas CILES"
git config --global user.email "thomas.ciles.dev@gmail.com"
```

## Create a new repository

```
git clone git@gitlab.com:Keyjuh/cube.git
cd cube
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

## Existing folder

```
cd existing_folder
git init
git remote add origin git@gitlab.com:Keyjuh/cube.git
git add .
git commit
git push -u origin master
```

## Existing Git repository

```
cd existing_repo
git remote add origin git@gitlab.com:Keyjuh/cube.git
git push -u origin --all
git push -u origin --tags
```