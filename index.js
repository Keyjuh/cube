'use strict';
const electron = require('electron');
const app = electron.app;
const userData = app.getPath('userData');


// adds debug features like hotkeys for triggering dev tools and reload
require('electron-debug')();

// prevent window being garbage collected
let mainWindow;

function onClosed() {
	// dereference the window
	// for multiple windows store them in an array
	mainWindow = null;
}

function createMainWindow() {
	const win = new electron.BrowserWindow({
		width: 1000,
		height: 750
	});

	win.loadURL(`file://${__dirname}/index.html`);
	win.on('closed', onClosed);
	// win.openDevTools();

	return win;
}

app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', () => {
	if (!mainWindow) {
		mainWindow = createMainWindow();
	}
});

app.on('ready', () => {
	mainWindow = createMainWindow();
});


/*
*	APP 
*/ 
const ipcMain = require('electron').ipcMain;
const nedb = require('nedb');
const db = new nedb({ filename: userData + '/database.db', autoload: true }); // load database


//========================================================== 
// Listen event send from sub process (public/js/appMain.js) 
//========================================================== 


// ==============================================================
// User
// ==============================================================
/**
* Check if user exists
* If no : Need a registration
* If yes: Find user with condition
* @param channel string event type
* @param event 
* @param doc object user data
*/
ipcMain.on('login', (event, doc) => {

	db.find({}, (err, result) => {
		if(err || result.length < 1) {
			// Aucun utilisateur
			event.sender.send('toRegister', ''); // goto register
		} else {
			// Un utilisateur a été trouvé
			db.find(doc, (err, result) => {
				if(err || result.length < 1) {
					result = false;
				} else {
					result = result.shift();
				}
				
				event.sender.send('login', result); // renvoi des données au sous processus => appMain.js
			});
		}	
	});
	
});


/**
 * Insert a new user
 * @param channel string event type
 * @param event 
 * @param doc object user data
 */
ipcMain.on('register', (event, doc) => {
	db.insert(doc, (err, newDoc) => {
		let inserted = newDoc;
		if(err) {
			inserted = false;
		}

		event.sender.send('register', inserted); // renvoi des données au sous processus => appMain.js
	});
});


/**
 * Update user to login, set session active (remember me => true)
 * @param channel string event type
 * @param event 
 * @param doc object user data
 */
ipcMain.on('userConnected', (event, doc) => {
	db.update({_id: doc._id}, doc, {}, (err) => {
		let updated = true;
		if(err) {
			updated = false;
		}
		event.sender.send('userConnected', updated);
	});
});


/**
 * Update user to logout, set session active (remember me => false)
 * @param channel string event type
 * @param event 
 * @param doc object user data
 */
ipcMain.on('logout', (event, doc) => {

	db.update({_id: doc._id}, doc, {}, (err) => {
		let updated = true;
		if(err) {
			updated = false;
		}
		event.sender.send('logout', updated);
	})

});


/**
 * Update user information
 * @param channel string event type
 * @param event 
 * @param doc object user data
 */
ipcMain.on('updateUser', (event, doc) => {

	db.update({_id: doc._id}, doc, {}, (err) => {
		let updated = true;
		if(err) {
			updated = false;
		}
		event.sender.send('updateUser', updated);
	})

});


/**
 * Delete user
 * @param channel string event type
 * @param event 
 * @param doc object user data
 */
ipcMain.on('deleteUser', (event, doc) => {

	db.remove({_id: doc._id}, (err) => {
		let deleted = true;
		if(err) {
			deleted = false;
		}
		event.sender.send('deleteUser', deleted);
	})

});




// ==============================================================
// Works sessions
// ==============================================================
/**
 * Insert into user project sessions array a new session
 * @param channel string event type
 * @param event 
 * @param doc object user data
 */
ipcMain.on('newSession', (event, doc) => {

	db.update({_id: doc._id}, doc, {}, (err) => {
		let updated = true;
		if(err) {
			updated = false;
		}
		event.sender.send('newSession', updated);
	});
});


/**
 * Delete into user project sessions array a specific session
 * @param channel string event type
 * @param event 
 * @param doc object user data
 */
ipcMain.on('deleteSession', (event, doc) => {

	db.update({_id: doc._id}, doc, {}, (err) => {
		let updated = true;
		if(err) {
			updated = false;
		}
		event.sender.send('deleteSession', updated);
	});
});


/**
 * Update into user project sessions array a specific session
 * @param channel string event type
 * @param event 
 * @param doc object user data
 */
ipcMain.on('updateSession', (event, doc) => {

	db.update({_id: doc._id}, doc, {}, (err) => {
		let updated = true;
		if(err) {
			updated = false;
		}
		event.sender.send('updateSession', updated);
	});
});




// ==============================================================
// Project
// ==============================================================
/**
 * Update into user projects array a specific project information
 * @param channel string event type
 * @param event 
 * @param doc object user data
 */
ipcMain.on('updateProject', (event, doc) => {

	db.update({_id: doc._id}, doc, {}, (err) => {
		let updated = true;
		if(err) {
			updated = false;
		}
		event.sender.send('updateProject', updated);
	});

});

/**
 * Delete into user projects array a specific project
 * @param channel string event type
 * @param event 
 * @param doc object user data
 */
ipcMain.on('deleteProject', (event, doc) => {

	db.update({_id: doc._id}, doc, {}, (err) => {
		let updated = true;
		if(err) {
			updated = false;
		}
		event.sender.send('deleteProject', updated);
	});
	
});