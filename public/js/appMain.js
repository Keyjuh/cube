const ipcRenderer = require('electron').ipcRenderer;
const smalltalk = require('smalltalk');
let myapp = angular.module('myapp', ['ngRoute']);


/*
    key mapping
*/ 
const P_1 = 38;
const P_2 = 233;
const P_3 = 34;
const P_4 = 40;
const P_5 = 167;
const P_6 = 39;

/*
    vars
*/ 
let user = {},
    timerOn = false,
    time_start = 0,
    time_end = 0,
    project_index = -1,
    notif_time = 0,
    interval = 1000,
    t_notif = null;

/*
    Routes configuration
*/
myapp.config(($routeProvider) => {

    $routeProvider
        .when('/', {
            templateUrl: 'views/home.html',
            controller : 'homeController'
        })
        .when('/logout', {
            templateUrl: 'views/home.html',
            controller : 'logoutController'
        })
        .when('/login', {
            templateUrl: 'views/login.html',
            controller : 'loginController'
        })
        .when('/register', {
            templateUrl: 'views/register.html',
            controller : 'registerController'
        })
        .when('/app', {
            templateUrl: 'views/app.html',
            controller : 'appController'
        })
        .when('/project/:id', {
            templateUrl: 'views/project.html',
            controller : 'projectController'
        })
        .when('/profile', {
            templateUrl: 'views/profile.html',
            controller : 'profileController'
        })

});


/**
 * Home controller
 */
myapp.controller('homeController', function($scope, $location) {
    $scope.goLogin = function() {
        $location.path('/login');
	};
});


/**
 * Login controller
 */
myapp.controller('loginController', function ($scope, $location) {

	document.body.classList = 'login';
	
    $scope.username = '';
    $scope.password = '';

    $scope.login = function() {        
    
        if(this.username != "" && this.password != "") {
            ipcRenderer.send('login', {
                username: this.username,
                password: this.password,
                active: false
            });
        }

    }
});


/**
 * Logout controller
 */
myapp.controller('logoutController', function ($scope, $location) {

	document.body.classList = 'logout';
	
    user.active = false;
    ipcRenderer.send('logout', user);    
});

/**
 * Register controller
 */
myapp.controller('registerController', function ($scope, $location) {
	
	document.body.classList = 'register';
    
    // default user data structure
    $scope.data = {
        username: '',
        email: '',
        password: '',
        projects: [
            {title: 'Nouveau projet 1', time: 0, sessions: []},
            {title: 'Nouveau projet 2', time: 0, sessions: []},
            {title: 'Nouveau projet 3', time: 0, sessions: []},
            {title: 'Nouveau projet 4', time: 0, sessions: []},
            {title: 'Nouveau projet 5', time: 0, sessions: []},
            {title: 'Nouveau projet 6', time: 0, sessions: []},
        ],
        active: true
    };

    $scope._password = '';

    $scope.register = function() {

        if(this.data.username != "" && this.data.email != "" && this.data.password != "" && this.data.password == this._password) {
            ipcRenderer.send('register', this.data);
        }

    }

});



//====================================================== 
// Controllers
//====================================================== 

/**
 * App controller
 */
myapp.controller('appController', function ($scope, $location) {
	
	document.body.classList = 'home';

    $scope.user = user;

	$scope.loadProject = function (id) {
		$location.path(`project/${id}`);
	};

    listenKeyboard();
});


/**
 * Projet controller
 */
myapp.controller('projectController', function ($scope, $location, $routeParams) {

	let color;
	switch ($routeParams.id) {
		case '0':
			color = 'one';
			break;
		case '1':
			color = 'two';
			break;
		case '2':
			color = 'three';
			break;
		case '3':
			color = 'four';
			break;
		case '4':
			color = 'five';
			break;
		case '5':
			color = 'six';
			break;
		default:
			color = 'zero';	
	}

	document.body.classList = 'project ' + color;

    $scope.user = user;
	$scope.project = $scope.user.projects[$routeParams.id];

    $scope.deleteSession = function(idSession) {
        smalltalk.confirm('Confirmation', 'Êtes vous sûr de vouloir supprimer la session ?').then(()=> {
            let old = user.projects[$routeParams.id].sessions[idSession];
            user.projects[$routeParams.id].time -= (old.end - old.start); // remove old session duration
            user.projects[$routeParams.id].sessions.splice(idSession, 1); // remove old session in array

            $scope.user = user; // update view $scope.vars
            $scope.project = $scope.user.projects[$routeParams.id];
            ipcRenderer.send('deleteSession', user); // send main process to update database
            window.location.href = `#!project/${$routeParams.id}`;
        }, ()=>{
            //no
        });
    }

    $scope.updateSession = function(idSession) {
        ipcRenderer.send('updateSession', user);
    }

    $scope.updateProject = function(idSession) {
        ipcRenderer.send('updateProject', user);
    }

    $scope.deleteProject = function(idSession) {
        smalltalk.confirm('Confirmation', 'Êtes vous sûr de vouloir supprimer le projet ?').then(()=> {
            user.projects[$routeParams.id].time = 0;
            user.projects[$routeParams.id].title = "Nouveau projet " + (parseInt($routeParams.id) + 1);
            user.projects[$routeParams.id].sessions = [];
            ipcRenderer.send('deleteProject', user);
        }, ()=>{
            //no
        });;
    }

});


/**
 * Profile controller
 */
myapp.controller('profileController', function($scope, $location) {

	document.body.classList = 'profile';
	
    $scope.user = user;

    $scope.updateUser = function() {
        user = this.user;
        ipcRenderer.send('updateUser', user);
    }

    $scope.deleteUser = function() {
        user = this.user;
        ipcRenderer.send('deleteUser', user);
    }

});




//====================================================== 
// Listen event send from main process (index.js) 
//====================================================== 

/**
 * Find if an active user is find
 */
ipcRenderer.send('login', { active: true });


/**
 * Receive event from main process login action return
 * @param channel string event type
 * @param event 
 * @param doc object user data
 */
ipcRenderer.on('login', (event, data) => {
    
    if(data === false) {
        // no user found
        window.location.href = '#!login';
    } else {
        // a user was found
        user = data;
        if(!user.active) {
            user.active = true;
            ipcRenderer.send('userConnected', user); // update db : set user active
        }

        window.location.href = '#!app';
    }

});

/**
 * Receive event from main process need a to register
 * @param channel string event type
 * @param event
 */
ipcRenderer.on('toRegister', (event) => {
    window.location.href = '#!register'; 
});

/**
 * Receive event from main process status of a new registration
 * @param channel string event type
 * @param event 
 * @param inserted boolean
 */
ipcRenderer.on('register', (event, inserted) => {
    if(inserted) {
        // success
        user = inserted;
        window.location.href = '#!app';
        notify({
            title: 'Information', 
            body : `Votre compte à bien été créé.`,
            silent: true
        });
    } else {
        // error
        notify({
            title: 'Error', 
            body : `Votre compte n'a pas pu être créé.`,
            silent: true
        });
    }
});


/**
 * Receive event from main process status of user is connected
 * @param channel string event type
 * @param event 
 * @param updated boolean
 */
ipcRenderer.on('userConnected', (event, updated) => {
    if(updated) {
        // success
    } else {
        // error
    }
});

/**
 * Receive event from main process status of a new session save
 * @param channel string event type
 * @param event 
 * @param updated boolean
 */
ipcRenderer.on('newSession', (event, updated) => {
    if(updated) {
        // success
        notify({
            title: 'Information', 
            body : `Votre session à bien été créée.`,
            silent: true
        });
    } else {
        // error
        notify({
            title: 'Error', 
            body : `Votre session rencontré une erreur.`,
            silent: true
        });
    }
});


/**
 * Receive event from main process status of a deleted session
 * @param channel string event type
 * @param event 
 * @param updated boolean
 */
ipcRenderer.on('deleteSession', (event, deleted) => {
    if(deleted) {
        // success
        notify({
            title: 'Information', 
            body : `Votre session a bien été supprimée.`,
            silent: true
        });
    } else {
        // error
        notify({
            title: 'Error', 
            body : `Votre session n'a pas pu être supprimée.`,
            silent: true
        });
    }
});


/**
 * Receive event from main process status of a modified session
 * @param channel string event type
 * @param event 
 * @param updated boolean
 */
ipcRenderer.on('updateSession', (event, updated) => {
    if(updated) {
        // success
        notify({
            title: 'Information', 
            body : `Votre session à bien été mis à jours.`,
            silent: true
        });
    } else {
        // error
        notify({
            title: 'Error', 
            body : `Votre session n'a pas pu être mis à jours.`,
            silent: true
        });
    }
});

/**
 * Receive event from main process status of a modified project
 * @param channel string event type
 * @param event 
 * @param updated boolean
 */
ipcRenderer.on('updateProject', (event, updated) => {
    if(updated) {
        // success
        notify({
            title: 'Information', 
            body : `Votre projet à bien été mis à jours.`,
            silent: true
        });
    } else {
        // error
        notify({
            title: 'Error', 
            body : `Votre profil n'a pas pu être mis à jours.`,
            silent: true
        });
    }
});

/**
 * Receive event from main process status of a deleted project
 * @param channel string event type
 * @param event 
 * @param updated boolean
 */
ipcRenderer.on('deleteProject', (event, deleted) => {
    if(deleted) {
        // success
        window.location.href = `#!app`;
        notify({
            title: 'Information', 
            body : `Votre projet à bien été supprimé.`,
            silent: true
        });
    } else {
        // error
        notify({
            title: 'Error', 
            body : `Le projet n'a pas pu être mis à jour.`,
            silent: true
        });
    }
});


/**
 * Receive event from main process status of user logout
 * @param channel string event type
 * @param event 
 * @param updated boolean
 */
ipcRenderer.on('logout', (event, logout) => {
    if(logout) {
        // success
        window.location.href = `#!login`;
    } else {
        // error
    }
});


/**
 * Receive event from main process status of a modified user
 * @param channel string event type
 * @param event 
 * @param updated boolean
 */
ipcRenderer.on('updateUser', (event, updated) => {
    if(updated) {
        // success
        window.location.href = `#!profile`;
        notify({
            title: 'Information', 
            body : `Votre profil à bien été mis à jours.`,
            silent: true
        });
    } else {
        // error
        notify({
            title: 'Error', 
            body : `Votre profil n'a pas pu être mis à jours.`,
            silent: true
        });
    }
});


/**
 * Receive event from main process status of a deleted user
 * @param channel string event type
 * @param event 
 * @param deleted boolean
 */
ipcRenderer.on('deleteUser', (event, deleted) => {
    if(deleted) {
        // success
        window.location.href = `#!register`;
    } else {
        // error
    }
});


/**
 * Create a system notification
 * @param options object
 * @return object the notification
 */
function notify(options) {
    return new Notification(options.title, options);
}

/**
 * Attach the keyboard listener
 */
function listenKeyboard() {

    $(document).keypress((e) => {

        if(!timerOn) {
            // Start session            
            switch(e.which) {
                case P_1:
                    initProjectSession(0);
                    break;
                case P_2:
                    initProjectSession(1);
                    break;
                case P_3:
                    initProjectSession(2);
                    break;
                case P_4:
                    initProjectSession(3);
                    break;
                case P_5:
                    initProjectSession(4);
                    break;
                case P_6:
                    initProjectSession(5);
                    break;
            }

        }
    });

    $(document).keyup((e) => {
        if(timerOn) {
            // End session
            endProjectSession();
            timerOn = false;
        }
    });

}


 /**
  * Start a new session init start time and set current project key
  * @param index int the current project array key
  */
function initProjectSession(index) {
    time_start = new Date().getTime();
    project_index = index;
    timerOn = true;
    
    makeTimerNotif();
}

 /**
  * Append html notification
  */
function makeTimerNotif() {
    let m = moment(notif_time);
    let notif = $(`<div class="notification timer">
        <p>${user.projects[project_index].title}</p>
        <div class="content">${m.minutes()} min ${m.seconds()} sec</div class="content">
    </div>`);
    $('#notifications').append(notif);

    updateNotif();
}

 /**
  * Update notification content
  */
function updateNotif() {
    let el = $('#notifications .timer .content');
    t_notif = setInterval(function() {
        m = moment(notif_time);
        notif_time += interval;        
        
        el.html(`${m.minutes()} min ${m.seconds()+1} sec`);
    }, interval);
}

 /**
  * Create the session and send data to update user
  */
function endProjectSession() {
    time_end = new Date().getTime();
    let duration = (time_end - time_start);
    let m = moment(duration).toObject();
    let session = {start: time_start, end: time_end, duration: duration, title: 'Nouvelle session', description: 'Description'}

    // update user data
    user.projects[project_index].time += duration;
    user.projects[project_index].sessions.push(session);

    // hide html notification
    $('#notifications').find('.timer').remove();
    clearInterval(t_notif);

    // Show project page
    window.location.href = `#!project/${project_index}`;

    // Update database
    ipcRenderer.send('newSession', user); // send data to index.js

    notify({
        title: user.projects[project_index].title, 
        body : `Durée de la session : ${m.minutes} min ${m.seconds} sec`,
        silent: true
    });

    // reset time and current project index
    project_index = -1;
    notif_time = 0;
}